package com.rikkei.tranning.ui;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView recyclerView;
    AppBarLayout appBarLayout;
    TextView textView;
    Toolbar toolbar;
    ArrayList<Student> mList;
    StudentAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mList = new ArrayList<>();
        collapsingToolbarLayout = findViewById(R.id.collapsingToolBarLayout2);
        recyclerView = findViewById(R.id.recyclerview2);
        appBarLayout = findViewById(R.id.appBarLayout2);
        textView = findViewById(R.id.text);
        toolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        recyclerView.setLayoutManager(layoutManager);

        // them thong tin sinh vien
        for (int i = 0; i < 10; i++ ){
            mList.add( new Student("Nguyễn Văn" + i, "Số nhà: " + i + 1 + ", đường Lê Đức Thọ"));
        }

        // recycler
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        adapter = new StudentAdapter(mList, Main2Activity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);

    }
}
