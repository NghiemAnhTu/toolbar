package com.rikkei.tranning.ui;


import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView imageView;
    AppBarLayout appBarLayout;
    Menu collapsedMenu;
    Boolean appBarEnable = true;
    StudentAdapter adapter;
    RecyclerView recyclerView;
    ArrayList<Student> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // anh xa

        recyclerView = findViewById(R.id.recyclerview);
        mList = new ArrayList<>();
        imageView = (ImageView) findViewById(R.id.image);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolBarLayout);
        appBarLayout = findViewById(R.id.appBarLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar( toolbar );

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        // them title
        collapsingToolbarLayout.setTitle("Android Desserts");
        collapsingToolbarLayout.setExpandedTitleColor( getResources().getColor(R.color.colorWhite));

        // them menu khi scrop

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if ( Math.abs( verticalOffset) > 300 ){
                    appBarEnable = false;
                    invalidateOptionsMenu();

                }else {
                    appBarEnable = true;
                    invalidateOptionsMenu();
                }
            }
        });

        // them thong tin sinh vien
        for (int i = 0; i < 10; i++ ){
            mList.add( new Student("Nguyễn Văn" + i, "Số nhà: " + i + 1 + ", đường Lê Đức Thọ"));
        }

        // recycler
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        adapter = new StudentAdapter(mList, MainActivity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        collapsedMenu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if ( collapsedMenu != null && !appBarEnable ){
            collapsedMenu.add("Menu 1").setIcon(R.mipmap.ic_launcher).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }else {

        }
        return super.onPrepareOptionsMenu(collapsedMenu);
    }

}
