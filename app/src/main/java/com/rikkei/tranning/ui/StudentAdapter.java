package com.rikkei.tranning.ui;



import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;



public class StudentAdapter extends RecyclerView.Adapter {
    ArrayList<Student> mList;
    Context mContext;

    public StudentAdapter(ArrayList<Student> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View view;
        TextView name, diachi;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            name = itemView.findViewById(R.id.name);
            diachi = itemView.findViewById(R.id.diachi);
            imageView = itemView.findViewById(R.id.image);

        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_student, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Student student = (Student) mList.get(i);
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.name.setText( student.getName() );
        holder.diachi.setText( student.getDiachi());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
